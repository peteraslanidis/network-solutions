# Network Solutions #
This is the Network Solution web design

[View client brief](clientBrief-vertical-tab-horizontal-roundcorner-menus.pdf)

## Design ##
The website was built in a complex layout.
## Slide Show ###
Once the page is loaded, a list is added to the page header below the navigation bar which has a set of images. 
The slideshow is then created using jQuery.
### Lightbox ###
Clicking on the navigation links it will display a popup lightbox with additional content.